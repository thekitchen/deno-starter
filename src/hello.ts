function hello(name?: string) {
  return `Hello ${name || "world"} !`;
}

export { hello };
