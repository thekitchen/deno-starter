import express, { Express, Request, Response } from "express";
import cors from "cors";

import { hello } from "./hello.ts";

class Server {
  #app: Express;

  constructor() {
    this.#app = express();
  }

  bootstrap() {
    this.#app.use(cors());
    this.#app.use(express.json());
    this.#app.use(
      express.urlencoded({
        extended: false,
      }),
    );

    this.#app.get("/hello", (req: Request, res: Response) => {
      res.send(hello(req.query.name as string));
    });

    return this.#app;
  }

  run() {
    this.#app.listen(8080);
    console.log("🚀 server started and available on http://localhost:8080");
  }
}

export { Server };
