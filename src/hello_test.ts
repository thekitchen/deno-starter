import { assertEquals } from "https://deno.land/std@0.181.0/testing/asserts.ts";
import { hello } from "./hello.ts";

Deno.test("should return hello world on call without name param", () => {
  assertEquals(hello(""), "Hello world !");
});

Deno.test("should return hello {name} on call with name param", () => {
  assertEquals(hello("John"), "Hello John !");
});
