import { Server } from "./server.ts";

const server = new Server();
await server.bootstrap();
server.run();
