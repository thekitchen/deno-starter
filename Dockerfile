FROM denoland/deno:latest as base

WORKDIR /app

COPY . ./

RUN deno cache src/server.ts

CMD ["deno", "run", "--allow-env", "--allow-read", "--allow-net", "./src/index.ts"] 